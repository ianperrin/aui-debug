/**
 * NOTE: This script is injected into every page (as specified by the manifest permissions).
 *
 * It has access to the DOM, but NOT to global scripts/variables in the host page.
 * This means that if you want jQuery, you have to include it yourself.
 * It also means you can't call AJS.anything as it doesn't exist for this sandboxed script.
 *
 * This script appears to run after everything has loaded, so DOM ready-ness is assumed
 */

/*

TO DO
* refactor errors to use type instead of crappy hacks for BODY
* refactor to use error/warn instead of log

IDEAS
* check for non-ADG colours
* do a layering/screening version so the page doesn't jump; and so we can screen out the page with blanket

NOTES
* Hard to accurately detect triggers for inline dialog, tooltips etc as they may be generated later
* Hard to detect overridden elements
* Rules are maintained manually, probably hard to extract; although could perhaps have a JSON file with key selectors, descriptions, etc.

*/

var $ = jQuery,
    auiVersion = false,
    auiDetected = false,
    validationLevel = "5.1";

function getAuiVersion() {
    return $('body').data("aui-version");
}

function isItAUI(options) {

    if ( !auiDetected ) {
        console.log("AUI Not Detected");
        return;
    }

    var auiComponents = [
            { "selector" : ".aui-avatar",                       "component" : "Avatar",             "type" : "block" },
            { "selector" : ".aui-badge",                        "component" : "Badge",              "type" : "inline" },
            { "selector" : ".aui-button",                       "component" : "Button",             "type" : "inline" },
            { "selector" : ".aui-buttons",                      "component" : "Button Group",       "type" : "block" },
            { "selector" : ".aui-dialog",                       "component" : "Dialog",             "type" : "block" },
            { "selector" : ".aui-dropdown",                     "component" : "Dropdown",           "type" : "block" },
            { "selector" : ".aui-dropdown2",                    "component" : "Dropdown2",          "type" : "block" },
            { "selector" : ".aui-expander-content",             "component" : "Expander Content",   "type" : "block" },
            { "selector" : ".aui-expander-trigger",             "component" : "Expander Trigger",   "type" : "block" },
            { "selector" : ".aui-group",                        "component" : "Group",              "type" : "block" },
            { "selector" : ".aui-header",                       "component" : "App Header",         "type" : "block" },
            { "selector" : ".aui-icon",                         "component" : "Icon",               "type" : "inline" },
            { "selector" : "#page #footer",                     "component" : "Footer",             "type" : "block" },
            { "selector" : ".aui-footer-list",                  "component" : "Footer list",        "type" : "block" },
            { "selector" : "form.aui",                          "component" : "Form",               "type" : "block" },
            { "selector" : ".aui-inline-dialog",                "component" : "Inline Dialog",      "type" : "block" },
            { "selector" : ".aui-message",                      "component" : "Message",            "type" : "block" },
            { "selector" : ".aui-label",                        "component" : "Label",              "type" : "inline" },
            { "selector" : ".aui-lozenge",                      "component" : "Lozenge",            "type" : "inline" },
            { "selector" : ".aui-navigation",                   "component" : "Navigation",         "type" : "inline" },
            { "selector" : ".aui-navgroup",                     "component" : "Navigation Group",   "type" : "block" },
            { "selector" : ".aui-page-header",                  "component" : "Page Header",        "type" : "block" },
            { "selector" : ".aui-page-panel",                   "component" : "Page Panel",         "type" : "block" },
            { "selector" : ".aui-progress-tracker",             "component" : "Progress Tracker",   "type" : "block" },
            { "selector" : ".ui-sidebar.ui-draggable",          "component" : "Sidebar",            "type" : "block" },
            { "selector" : ".aui-tabs",                         "component" : "Tabs",               "type" : "block" },
            { "selector" : "table.aui",                         "component" : "Table",              "type" : "block" },
            { "selector" : ".aui-toolbar",                      "component" : "Toolbar",            "type" : "block" },
            { "selector" : ".aui-toolbar2",                     "component" : "Toolbar2",           "type" : "block" },
            { "selector" : "[original-title]",                  "component" : "Tooltip",            "type" : "inline" },
            { "selector" : ".aui-theme-default",                "component" : "AUI Page",           "type" : "body" }
        ];

    if (options.showClasses !== false) {
        $("body").addClass("aui-debug-showclasses");
    }

    $.each(auiComponents, function(i,v) {
        $(auiComponents[i].selector)
            .attr("data-auidebug-component",auiComponents[i].component)
            .attr("data-auidebug-componenttype",auiComponents[i].type)
            ;
    });

    if ( $("[data-auidebug-component]").length ) {
        console.log("AUI components highlighted in page");
    } else {
        console.log("No AUI components detected");
    }

}

function isItBroken(options) {
    if ( !auiDetected ) {
        console.log("AUI Debug: AUI Not Detected - if it's meant to be here, it's VERY broken!");
        return;
    }

    var option = options || {};
    var brokenThings = [
        {
            "selector" :    ".aui-page-panel .aui-page-panel",
            "error":        "aui-page-panel should not be nested",
            "severity":     "error",
            "type":         "block"
        },
        {
            "selector" :    ".aui-dd-parent",
            "error":        "Dropdown1 is deprecated",
            "severity":     "error",
            "type":         "hidden"
        },
        {
            "selector" :    ".aui-toolbar",
            "error":        "Toolbar1 is deprecated",
            "severity":     "error",
            "type":         "block"
        },
        {
            "selector" :    ".aui-layout",
            "error":        "aui-layout is no longer required on BODY",
            "severity":     "warning",
            "type":         "block"
        },
        {
            "selector" :    ".aui-theme-default",
            "error":        "aui-theme-default is no longer required on BODY",
            "severity":     "warning",
            "type":         "block"
        },
        {
            "selector" :    ".svg-icon",
            "error":        "AUI SVG icons were removed in 5.1",
            "severity":     "warning",
            "type":         "block"
        },
        {
            "selector" :    ".aui-nav-current",
            "error":        ".aui-nav-current deprecated, use .aui-nav-selected instead",
            "severity":     "warning",
            "type":         "block"
        },
        {
            "selector" :    "#firebug-warning",
            "error":        "#firebug-warning is deprecated, no longer works and should be removed.",
            "severity":     "error",
            "type":         "block"
        },
        {
            "selector" :    ".aui-toolbar-2-inner",
            "error":        ".aui-toolbar-2-inner deprecated, use .aui-toolbar2-inner instead",
            "severity":     "warning",
            "type":         "block"
        },
        {
            "selector" :    "div.aui-avatar",
            "error":        ".aui-avatar should be changed from DIV to SPAN",
            "severity":     "warning",
            "type":         "block"
        },
        {
            "selector" :    "div.aui-avatar-inner",
            "error":        ".aui-avatar-inner should be changed from DIV to SPAN",
            "severity":     "warning",
            "type":         "block"
        }
    ];

    // Test for aui items not contained in a group
    $(".aui-item").each(function(i) {
        if ( $(this).parent(".aui-group").length === 0) {
            console.warn("AUI debug ERROR  :", ".aui-item should be in a group", this);
        }
    });

    // Test for aui icons
    $(".aui-icon").each(function(i) {
        if ( !$(this).text().length && !$(this).parent().text().length ) {
            console.warn("AUI debug ERROR  :", "Icon and Parent both missing accessible text", this);
        }
    });

    // console.log(brokenThings);

    $.each(brokenThings, function(i,v) {
        var btSelecter = brokenThings[i].selector,
            btSelecterMessage = brokenThings[i].selectorMessage || btSelecter,
            btError = brokenThings[i].error,
            btSeverity = brokenThings[i].severity,
            btSeverityMessage = "ERROR  ",
            btType = brokenThings[i].type,
            btOverlays = options.overlays || false,
            showErrors = options.showErrors || false,
            btMessage,
            currentMessage
            ;

        if ( btSeverity == "warning") {
            btSeverityMessage = "WARNING";
        }
        btMessage = btSeverityMessage + ": " + btError;

        if ( $(btSelecter).length ) {
            console.log("AUI Debug " + btMessage + " (" + btSelecterMessage + ")");
        }

        if ( showErrors === true) {
            currentMessage = $(btSelecter).attr("data-auidebug-error");
            if ( currentMessage ) {
                btMessage = currentMessage + " " + btMessage;
            }
            $(btSelecter).attr("data-auidebug-error", btMessage);
        }

        if ( btOverlays ) {

            $.each( $(btSelecter), function() {
                console.log("selector: " + btSelecter);
                console.log($(this));
                var message = $('<div class="aui-debug-error"></div>'),
                    eloffset = $(this).offset(),
                    elheight = $(this).height(),
                    elwidth = $(this).width(),
                    elposition = "absolute";

                console.log($(this).prop("tagName"));

                if ( $(this).prop("tagName") == "BODY") {
                    elheight = "auto";
                }

                message.css({
                    position: elposition,
                    top: eloffset.top,
                    left: eloffset.left,
                    height: elheight,
                    width: elwidth
                });

                message.text(btError);

                if ( $(this).prop("tagName") == "BODY") {
                    message.prependTo("body");
                } else {
                    message.appendTo("body");
                }

            });
        }

    });
    console.log("AUI Debug finished");
}

function cleanUp(options) {
    console.log("Running cleanup");
    var attrs = [
        "data-auidebug-component",
        "data-auidebug-componenttype",
        "data-auidebug-error"
        ];

    $.each(attrs, function(i,v) {
        $("[" + v + "]").removeAttr(v);
    });

    $(".aui-debug-error").remove();
    $(".aui-debug-message").remove();
    $("body").removeClass("aui-debug-showclasses");
    console.clear();
}

if ( window.location.protocol == "file:" ) {

    console.log("ERROR: published AUI Debugger does not work for local files, please view via http/s.");
   
} else {

    auiVersion = getAuiVersion();
    auiDetected = ( auiVersion ) ? true : false;

    if ( auiDetected ) {
        $("#auiversion").text(auiVersion);
        // console.log("AUI Debug: AUI " + auiVersion + " detected. Compatibility rules applied: " + validationLevel);
        console.log("AUI Debug: AUI " + auiVersion + " detected. Use debugger icon to identify and validate usage.");
    } else {
        console.log("AUI Debugger: AUI not detected. Debug can be run manually.");
    }

}

jQuery(document).ready( function() {

    jQuery("#aui-debug-all").click( function(e) {
        chrome.tabs.executeScript(null, {code: 'isItAUI({ "showClasses" : true })' });
        chrome.tabs.executeScript(null, {code: 'isItBroken({ "showErrors" : true, "btOverlays": true })' });
    });
    jQuery("#aui-debug-isItAUI").click( function(e) {
        chrome.tabs.executeScript(null, {code: 'isItAUI({ "showClasses" : true })' });
    });
    jQuery("#aui-debug-isItBroken").click( function(e) {
        chrome.tabs.executeScript(null, {code: 'isItBroken({ "showErrors" : true, "btOverlays": true })' });
    });
    jQuery("#aui-debug-reset").click( function(e) {
        chrome.tabs.executeScript(null, {code: 'cleanUp()' });
    });

});
